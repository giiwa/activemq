package org.giiwa.activemq.web.admin;

import java.util.concurrent.atomic.AtomicLong;

import javax.jms.JMSException;

import org.giiwa.activemq.mq.IStub;
import org.giiwa.activemq.mq.MQ;
import org.giiwa.core.bean.TimeStamp;
import org.giiwa.core.conf.Global;
import org.giiwa.core.json.JSON;
import org.giiwa.core.task.Task;

public class test {

  public static void main(String[] args) {

    Global.setConfig("activemq.url",
        "failover:(tcp://joe.mac:61616)?timeout=3000&jms.prefetchPolicy.all=2&jms.useAsyncSend=true");
    Global.setConfig("activemq.group", "demo");

    Task.init(200, null);

    if (MQ.init()) {

      TimeStamp t2 = TimeStamp.create();
      int n = 10000;
      int c = 100;
      Tester[] t = new Tester[c];
      for (int i = 0; i < t.length; i++) {
        t[i] = new Tester("t" + i, n);
        try {
          t[i].bind();
        } catch (JMSException e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }
        t[i].send("echo", JSON.create());
      }

      synchronized (t) {
        int i = 0;
        try {
          for (Tester t1 : t) {
            while (!t1.isFinished() && i < 100) {
              t.wait(1000);
              i++;
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }

      for (Tester t1 : t) {
        t1.println();
      }

      System.out.println("TPS: " + (n * c) * 1000 / t2.past());
    }

    System.exit(0);

  }

  public static class Tester extends IStub {

    int        n;
    AtomicLong seq     = new AtomicLong();
    AtomicLong back    = new AtomicLong();
    AtomicLong total   = new AtomicLong();

    JSON       status  = JSON.create();
    JSON       msg;
    String     to;
    long       created = System.currentTimeMillis();

    public Tester(String name, int n) {
      super(name);
      this.n = n;
    }

    public void println() {
      System.out.println(status.toString());
    }

    public void send(String to, JSON msg) {
      this.msg = msg;
      this.to = to;

      long s = seq.incrementAndGet();
      msg.put("sendtime", System.currentTimeMillis());

      try {
        this.send(s, to, msg.toString().getBytes());
      } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    public boolean isFinished() {
      return back.get() == n;
    }

    @Override
    public void onRequest(long seq, String from, byte[] message) {
      // System.out.println(msg);

      long min = status.getLong("min", Long.MAX_VALUE);
      long max = status.getLong("max", Long.MIN_VALUE);

      long t = System.currentTimeMillis() - msg.getLong("sendtime");
      total.addAndGet(t);
      if (t < min) {
        status.put("min", t);
      }
      if (t > max) {
        status.put("max", t);
      }
      status.put("total", total.get());

      back.incrementAndGet();
      status.put("aver", total.get() / back.get());

      if (this.seq.get() < n) {
        send(this.to, msg);
      }

      if (back.get() == n) {
        status.put("duration", System.currentTimeMillis() - created);
      }
    }

  }
}

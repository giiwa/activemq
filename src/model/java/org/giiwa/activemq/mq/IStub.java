package org.giiwa.activemq.mq;

import javax.jms.JMSException;

import org.giiwa.activemq.mq.MQ.Mode;
import org.giiwa.activemq.mq.MQ.Receiver;

/**
 * the message stub
 * 
 * @author joe
 *
 */
public abstract class IStub {

  private Receiver r;
  protected String name;

  public IStub(String name) {
    this.name = name;
  }

  final public void bind() throws JMSException {
    bind(Mode.QUEUE);
  }

  final public void bind(Mode m) throws JMSException {
    r = MQ.bind(name, this, m);
  }

  final public void close() {
    if (r != null) {
      r.close();
    }
  }

  public void send(long seq, String to, byte[] message) throws Exception {
    MQ.send(seq, to, message, name);
  }

  public abstract void onRequest(long seq, String from, byte[] message);

}

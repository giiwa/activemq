package org.giiwa.activemq.mq;

import java.util.HashMap;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.giiwa.activemq.web.admin.activemq;
import org.giiwa.core.bean.TimeStamp;
import org.giiwa.core.bean.X;
import org.giiwa.core.conf.Global;
import org.giiwa.core.task.Task;
import org.giiwa.framework.bean.OpLog;

/**
 * the distribute message system, <br>
 * the performance: sending 1w/300ms <br>
 * recving 1w/1500ms<br>
 * 
 * @author joe
 *
 */
public final class MQ {

  private static String group = X.EMPTY;

  private static Log    log   = LogFactory.getLog(MQ.class);

  /**
   * the message stub type <br>
   * TOPIC: all stub will read it <br>
   * QUEUE: only one will read it
   * 
   * @author joe
   *
   */
  public static enum Mode {
    TOPIC, QUEUE
  };

  private static Connection                connection;
  private static Session                   session;
  private static String                    url;       // failover:(tcp://localhost:61616,tcp://remotehost:61616)?initialReconnectDelay=100
  private static String                    user;
  private static String                    password;
  private static ActiveMQConnectionFactory factory;

  public static boolean init() {
    if (session == null) {
      try {

        if (session != null)
          return true;

        url = Global.getString("activemq.url", ActiveMQConnection.DEFAULT_BROKER_URL);
        user = Global.getString("activemq.user", ActiveMQConnection.DEFAULT_USER);
        password = Global.getString("activemq.passwd", ActiveMQConnection.DEFAULT_PASSWORD);

        group = Global.getString("activemq.group", X.EMPTY);
        if (!group.endsWith(".")) {
          group += ".";
        }

        if (factory == null) {
          factory = new ActiveMQConnectionFactory(user, password, url);
        }

        if (connection == null) {
          connection = factory.createConnection();
          connection.start();
        }

        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        OpLog.info(activemq.class, "startup", "connected ActiveMQ with [" + url + "]", null, null);

      } catch (Throwable e) {
        log.error(e.getMessage(), e);
        // e.printStackTrace();
        OpLog.warn(activemq.class, "startup", "failed ActiveMQ with [" + url + "]", null, null);
      }
    }

    return session != null;
  }

  private MQ() {
  }

  /**
   * listen on the name
   * 
   * @param name
   * @param stub
   * @throws JMSException
   */
  public static Receiver bind(String name, IStub stub, Mode mode) throws JMSException {
    if (session == null)
      throw new JMSException("MQ not init yet");

    OpLog.info(activemq.class, "bind", "[" + name + "], stub=" + stub.getClass().toString() + ", mode=" + mode, null,
        null);

    return new Receiver(name, stub, mode);
  }

  public static Receiver bind(String name, IStub stub) throws JMSException {
    return bind(name, stub, Mode.QUEUE);
  }

  /**
   * QueueTask
   * 
   * @author joe
   * 
   */
  public final static class Receiver implements MessageListener {
    String          name;
    IStub           cb;
    MessageConsumer consumer;
    TimeStamp       t     = TimeStamp.create();
    int             count = 0;

    public void close() {
      if (consumer != null) {
        try {
          consumer.close();
        } catch (JMSException e) {
          log.error(e.getMessage(), e);
        }
      }
    }

    private Receiver(String name, IStub cb, Mode mode) throws JMSException {
      this.name = name;
      this.cb = cb;

      if (session != null) {
        Destination dest = null;
        if (mode == Mode.QUEUE) {
          dest = new ActiveMQQueue(group + name);
        } else {
          dest = new ActiveMQTopic(group + name);
        }

        consumer = session.createConsumer(dest);
        consumer.setMessageListener(this);

      } else {
        log.warn("MQ not init yet!");
        throw new JMSException("MQ not init yet!");
      }
    }

    @Override
    public void onMessage(Message m) {
      try {
        // System.out.println("got a message.., " + t.reset() +
        // "ms");

        count++;
        if (m instanceof BytesMessage) {
          BytesMessage m1 = (BytesMessage) m;
          process(name, m1, cb);
        } else {
          System.out.println(m);
        }

        if (count % 10000 == 0) {
          System.out.println("process the 10000 messages, cost " + t.reset() + "ms");
        }

      } catch (Exception e) {
        log.error(e.getMessage(), e);
      }
    }
  }

  private static void process(final String stub, final BytesMessage req, final IStub cb) {

    new Task() {
      @Override
      public void onExecute() {

        try {

          long seq = req.readLong();
          int len = req.readInt();
          String from = null;
          if (len > 0) {
            byte[] ff = new byte[len];
            req.readBytes(ff);
            from = new String(ff);
          }
          len = req.readInt();
          byte[] message = null;
          if (len > 0) {
            message = new byte[len];
            req.readBytes(message);
          }
          log.debug("got a message: from=" + from + ", len=" + len);

          cb.onRequest(seq, from, message);

        } catch (Exception e) {
          log.error(e.getMessage(), e);
        }
      }
    }.schedule(0);

  }

  /**
   * broadcast the message as "topic" to all "dest:to", and return immediately
   * 
   * @param seq
   * @param to
   * @param message
   * @param from
   * @return 1: success<br>
   * @throws Exception
   */
  public static int broadcast(long seq, String to, byte[] message, String from) throws Exception {
    if (X.isEmpty(message))
      throw new Exception("message can not be empty");

    if (session == null) {
      throw new Exception("MQ not init or not successful init");
    }

    /**
     * get the message producer by destination name
     */
    MessageProducer p = getTopic(to);
    if (p != null) {
      BytesMessage req = session.createBytesMessage();

      req.writeLong(seq);
      byte[] ff = from == null ? null : from.getBytes();
      if (ff == null) {
        req.writeInt(0);
      } else {
        req.writeInt(ff.length);
        req.writeBytes(ff);
      }
      if (message == null) {
        req.writeInt(0);
      } else {
        req.writeInt(message.length);
        req.writeBytes(message);
      }

      p.send(req, DeliveryMode.NON_PERSISTENT, 0, X.AMINUTE);

      log.debug("Broadcasting:" + to + ", len=" + message.length);

      return 1;
    }

    return -1;
  }

  /**
   * send the message and return immediately
   * 
   * @param seq
   * @param to
   * @param message
   * @param bb
   * @param from
   * @return int 1: success
   */
  public static int send(long seq, String to, byte[] message, String from) throws Exception {
    if (X.isEmpty(message))
      throw new Exception("message can not be empty");

    if (session == null) {
      throw new Exception("MQ not init or not successful init");
    }

    /**
     * get the message producer by destination name
     */
    MessageProducer p = getQueue(to);
    if (p != null) {
      BytesMessage req = session.createBytesMessage();

      req.writeLong(seq);
      byte[] ff = from == null ? null : from.getBytes();
      if (ff == null) {
        req.writeInt(0);
      } else {
        req.writeInt(ff.length);
        req.writeBytes(ff);
      }
      if (message == null) {
        req.writeInt(0);
      } else {
        req.writeInt(message.length);
        req.writeBytes(message);
      }

      p.send(req, DeliveryMode.NON_PERSISTENT, 0, X.AMINUTE);

      log.debug("Sending:" + to + ", len=" + (message == null ? 0 : message.length));

      return 1;
    }

    return -1;
  }

  /**
   * 获取消息队列的发送庄
   * 
   * @param name
   *          消息队列名称
   * @return messageproducer
   */
  private static MessageProducer getQueue(String name) {
    synchronized (queues) {
      if (session != null) {
        if (queues.containsKey(name)) {
          return queues.get(name);
        }

        try {
          Destination dest = new ActiveMQQueue(group + name);
          MessageProducer producer = session.createProducer(dest);
          producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
          queues.put(name, producer);

          return producer;
        } catch (Exception e) {
          log.error(name, e);
        }
      }
    }

    return null;
  }

  private static MessageProducer getTopic(String name) {
    synchronized (topics) {
      if (session != null) {
        if (topics.containsKey(name)) {
          return topics.get(name);
        }

        try {
          Destination dest = new ActiveMQTopic(group + name);
          MessageProducer producer = session.createProducer(dest);
          producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
          topics.put(name, producer);

          return producer;
        } catch (Exception e) {
          log.error(name, e);
        }
      }
    }
    return null;
  }

  /**
   * queue producer cache
   */
  private static Map<String, MessageProducer> queues = new HashMap<String, MessageProducer>();

  /**
   * topic producer cache
   */
  private static Map<String, MessageProducer> topics = new HashMap<String, MessageProducer>();

}

package org.giiwa.activemq.mq;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.giiwa.core.json.JSON;
import org.giiwa.framework.web.Tps;

public class Echo extends IStub {

  static Log log = LogFactory.getLog(Echo.class);

  public Echo(String name) {
    super(name);
  }

  @Override
  public void onRequest(long seq, String from, byte[] message) {
    try {
      JSON msg = JSON.fromObject(message);
      msg.put("gottime", System.currentTimeMillis());
      MQ.send(seq, from, msg.toString().getBytes(), name);
      Tps.add(1);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }
  }

}
